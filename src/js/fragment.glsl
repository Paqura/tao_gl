varying vec2 vUv;
varying vec2 vUv_second;
uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform vec2 u_pixels;
uniform vec2 u_uvrate_one;
uniform vec2 u_accel;
uniform float u_progress;
uniform float u_time;
uniform sampler2D u_texture_f;
uniform sampler2D u_texture_s;

/* Закрыте дыры на маленьких разрешениях */

vec2 mirrored(vec2 v) {
  vec2 m = mod(v, 2.);
  return mix(m, 2.0 - m, step(1.0, m));
}

float tri(float p) {
  return mix(p, 1.0 - p, step(0.5, p))* 2.;
}

void main() { 
  vec2 uv = gl_FragCoord.xy/u_pixels.xy;
  
  float pos = fract(u_progress);
  float delayValue = pos*7. - uv.y*2. + uv.x - 2.;


  delayValue = clamp(delayValue, 0., 1.);

  vec2 translateValue = pos + delayValue*u_accel;
  vec2 translateValue_one = vec2(-0.5, 1.) * translateValue;
  vec2 translateValue_two = vec2(-0.5, 1.) * (translateValue - 1. - u_accel);

  vec2 wave = sin(sin(u_time) * vec2(0, 0.3) + vUv.yx*vec2(0, 4.))*vec2(0, 0.5);

  vec2 xy = wave*(tri(pos)*0.5 + tri(delayValue)*0.5);

  vec2 uv_one = vUv_second + translateValue_one + xy;
  vec2 uv_two = vUv_second + translateValue_two + xy;

  vec4 rgba_one = texture2D(u_texture_f, mirrored(uv_one));
  vec4 rgba_two = texture2D(u_texture_s, mirrored(uv_two));

  /**
  * В зависимости от прогресс(0 или 1)
  * меняется текстура rgba_one или rgba_two
  */

  vec4 rgba = mix(rgba_one, rgba_two, delayValue);
  gl_FragColor = rgba;
}