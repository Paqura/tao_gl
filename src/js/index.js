import * as THREE from 'three';
const OrbitControls = require('three-orbit-controls')(THREE);
import fragment from './fragment.glsl';
import vertex from './vertex.glsl';
import {TimelineMax} from 'gsap';

/**
 * * container: див, в который пихаем канвас
 * * camera, scene, renderer - всё, как обычно
 * * uniforms - по сути шейдеры с параметрами, далее подробней
 */

let container;
let camera,
  scene,
  renderer,
  geometry,
  material,
  mesh;
let uniforms,
  controls;
let time = 0;
let tl = new TimelineMax();
const body = document.querySelector('body');
// const name_f = document.querySelector('.name .f'); const name_s =
// document.querySelector('.name .s');
const loader = new THREE.TextureLoader();

const assets = [
  {
    name: 'texture_f',
    path: './img/img1.jpg'
  }, {
    name: 'texture_s',
    path: './img/img2.jpg'
  }, {
    name: 'texture_t',
    path: './img/img3.jpg'
  },
  {
    name: 'texture_4',
    path: './img/img4.jpg'
  },
  {
    name: 'texture_5',
    path: './img/img5.jpg'
  },
  {
    name: 'texture_6',
    path: './img/img6.jpg'
  }
];

let textures = [];

/**
 * * @param { paths: array<Paths> }
 * * получает массив из имен и путей
 * * по окончании загрузки запускает функции
 * * инициализации
 */

function loadImages(paths) {
  let arr = paths;
  arr.forEach(it => {
    loader
      .load(it.path, function (texture) {
        textures.push(texture);
        arr.splice(arr.indexOf(it), 1);
        if (!arr.length) {
          init();
          animate();
        }
      })
  });
}

loadImages(assets);

function init() {
  container = document.querySelector('.app');

  scene = new THREE.Scene();
  renderer = new THREE.WebGLRenderer();

  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  container.appendChild(renderer.domElement);

  camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.001, 100);

  camera
    .position
    .set(0, 0, 1);

  // controls = new OrbitControls(camera, renderer.domElement);

  geometry = new THREE.PlaneGeometry(1, 1, 1, 1);

  /**
   * * uniforms
   * * каждый объект - название переменной в шейдерах
   * * @param { type: 'f' } float
   * * @params { type: 'v2 } Vector2 - двумерный вектор
   */

  uniforms = {
    u_accel: {
      type: 'v2',
      value: new THREE.Vector2(0.5, 2)
    },
    u_time: {
      type: "f",
      value: 1.0
    },
    u_pixels: {
      type: 'v2',
      value: new THREE.Vector2(window.innerWidth, window.innerHeight)
    },
    u_animation: {
      type: "f",
      value: 0.0
    },
    u_uvrate_one: {
      value: new THREE.Vector2(1, 1)
    },
    u_progress: {
      type: 'f',
      value: 0.0
    },
    u_resolution: {
      type: "v2",
      value: new THREE.Vector2()
    },
    u_size: {
      type: "v2",
      value: new THREE.Vector2()
    },
    u_texture_f: {
      value: textures[0]
    },
    u_texture_s: {
      value: textures[1]
    }
  };

  /**
   * * Создание материала,
   * * шейдеры тащатся из хтмл
   */

  material = new THREE.ShaderMaterial({side: THREE.DoubleSide, uniforms, wireframe: false, vertexShader: vertex, fragmentShader: fragment});

  mesh = new THREE.Mesh(geometry, material);
  scene.add(mesh);
  onWindowResize();
  window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize(event) {
  var w = window.innerWidth;
  var h = window.innerHeight;
  renderer.setSize(w, h);
  camera.aspect = w / h;

  material.uniforms.u_uvrate_one.value.y = h / w;

  // calculate scene
  let dist = camera.position.z - mesh.position.z;
  let height = 1;
  camera.fov = 2 * (180 / Math.PI) * Math.atan(height / (2 * dist));
  mesh.scale.x = w / h;

  camera.updateProjectionMatrix();
}

function animate() {
  time = time + 0.05;
  uniforms.u_time.value = time;
  requestAnimationFrame(animate);
  render();
  raf();
}

function render() {
  renderer.render(scene, camera);
}

/** SCROLLER */

let speed = 0;
let position = 0;

document.addEventListener('wheel', evt => {
  speed += evt.deltaY * 0.00042;
});

function raf() {
  let currentSlide,
      nextSlide;

  position += speed;
  speed *= 0.7;

  let i = Math.round(position);
  let dif = i - position;

  position += dif * 0.035;
  if (Math.abs(i - position) < 0.002) {
    position = i;
  }

  currentSlide = (Math.floor(position) - 1 + textures.length) % textures.length;
  nextSlide = (((Math.floor(position) + 1) % textures.length -1) + textures.length) % textures.length;

  material.uniforms.u_texture_f.value = textures[currentSlide];
  material.uniforms.u_texture_s.value = textures[nextSlide];

  window.c = currentSlide;
  window.n = nextSlide;
  material.uniforms.u_progress.value = position;
}
