uniform float time;
varying vec2 vUv;
varying vec2 vUv_second;
varying vec2 vUv1;
varying vec4 vPosition;

uniform sampler2D u_texture_f;
uniform sampler2D u_texture_s;
uniform vec2 u_pixels;
uniform vec2 u_uvrate_one;

void main() {
  // Реализация background-size: cover в шейдере
  vUv = uv;
  vec2 _uv = uv - 0.5;
  vUv_second = _uv;
  vUv_second *= u_uvrate_one;

  vUv_second += 0.5;

  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}